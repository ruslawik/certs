<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPersonRequest;
use App\Http\Requests\StorePersonRequest;
use App\Http\Requests\UpdatePersonRequest;
use App\Models\Certificate;
use App\Models\Person;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Excel;
use App\Imports\ExcelImport;
use Yajra\DataTables\Facades\DataTables;

class PeopleController extends Controller
{   

    public function excelImport (Request $r){

        $r->validate([
            'excel' => 'required|mimes:xlx,xls,xlsx|max:4048'
        ]);

        $cert_data = Certificate::where('id', $r->input('cert_id'))->get();

        if($r->file()) {
            $fileName = time().'_'.$r->excel->getClientOriginalName();
            $filePath = $r->file('excel')->storeAs('excels', $fileName, 'public');

            $rows = Excel::toArray(new ExcelImport, storage_path('/app/public/' . $filePath));
            $cert_id = $cert_data[0]->id;
            $file_to_delete = storage_path('/app/public/' . $filePath);

            unlink($file_to_delete);

            foreach ($rows[0] as $row) {
                $fio = $row[0];
                $email = $row[1];
                $person = new Person();
                $person->name = $fio;
                $person->email = $email;
                $person->certificate_id = $cert_id;
                $person->save();
            }

            return redirect('/admin/people');
        }
    }

    /*
    public function loadImport (Request $r){

        $all_people = $r->input('all_people');
        $cert_id = $r->input('cert_id');

        for($i=0; $i<$all_people-1; $i++){
            $fio = $r->people_name[$i];
            $email = $r->people_email[$i];
            $person = new Person();
            $person->name = $fio;
            $person->email = $email;
            $person->certificate_id = $cert_id;
            $person->save();
        }

        return redirect('/admin/people');
    }*/

    public function index(Request $request)
    {

        if ($request->ajax()) {
            $query = Person::query()->select(sprintf('%s.*', (new Person)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'person_show';
                $editGate      = 'person_edit';
                $deleteGate    = 'person_delete';
                $crudRoutePart = 'people';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('cert_num', function ($row) {
                return $row->cert_num ? $row->cert_num : "";
            });
            $table->editColumn('bergen_date', function ($row) {
                return $row->bergen_date ? $row->bergen_date : "";
            });
            $table->editColumn('certificate_id', function ($row) {
                return $row->certificate_id ? $row->certificate_id : "";
            });


            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        $certificates = Certificate::get();

        return view('admin.people.index', compact('certificates'));

        /*
        abort_if(Gate::denies('person_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $people = Person::with(['certificate'])->get();

        $certificates = Certificate::get();

        return view('admin.people.index', compact('people', 'certificates'));*/
    }

    public function create()
    {
        abort_if(Gate::denies('person_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certificates = Certificate::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.people.create', compact('certificates'));
    }

    public function store(StorePersonRequest $request)
    {
        $person = Person::create($request->all());

        return redirect()->route('admin.people.index');
    }

    public function edit(Person $person)
    {
        abort_if(Gate::denies('person_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $certificates = Certificate::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $person->load('certificate');

        return view('admin.people.edit', compact('certificates', 'person'));
    }

    public function update(UpdatePersonRequest $request, Person $person)
    {
        $person->update($request->all());

        return redirect()->route('admin.people.index');
    }

    public function show(Person $person)
    {
        abort_if(Gate::denies('person_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $person->load('certificate');

        return view('admin.people.show', compact('person'));
    }

    public function destroy(Person $person)
    {
        abort_if(Gate::denies('person_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $person->delete();

        return back();
    }

    public function massDestroy(MassDestroyPersonRequest $request)
    {
        Person::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Workerman\Worker;

class BroadcastController extends Controller
{
    public function broadcast($video_id, $width, $height){

    	$ar['video_id'] = $video_id;
    	$ar['width'] = $width;
    	$ar['height'] = $height;

    	return view("broadcast", $ar);
    }

    public function broadcastEventon($video_id, $width, $height, $stream_id, $bearer_token){

    	$ar['video_id'] = $video_id;
    	$ar['width'] = $width;
    	$ar['height'] = $height;
    	$ar['stream_id'] = $stream_id;
    	$ar['bearer_token'] = $bearer_token;

    	return view("broadcast_eventon", $ar);
    }

    public function call($peer_id){

    	return view("call", ["peer_id" => $peer_id]);
    }

    public function stream(){

    	return view("stream");
    }

    public function audioServer(){

		// Create a Websocket server
		$ws_worker = new Worker('websocket://127.0.0.1:2346');

		// Emitted when new connection come
		$ws_worker->onConnect = function ($connection) {
		    echo "New connection\n";
		};

		// Emitted when data received
		$ws_worker->onMessage = function ($connection, $data) {
		    // Send hello $data
		    $connection->send('Hello ' . $data);
		};

		// Emitted when connection closed
		$ws_worker->onClose = function ($connection) {
		    echo "Connection closed\n";
		};

		// Run worker
		Worker::runAll();
    }
}

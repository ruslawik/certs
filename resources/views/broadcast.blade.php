<script src="/hjplayer.js"></script>

<script>

// Loads the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replaces the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
        height: '{{$height}}',
        width: '{{$width}}',
        videoId: '{{$video_id}}',
        playerVars: {
          autoplay: 1,
          controls: 1,
          disablekb: 1,
          hl: 'ru-ru',
          loop: 1,
          modestbranding: 1,
          showinfo: 0,
          autohide: 1,
          color: 'white',
          iv_load_policy: 3,
          theme: 'light',
          rel: 0
        }
        /*events: {
            'onReady': onPlayerReady,
        }*/
    });
  }

translation = undefined;

function change_lang(){

    //Если выбран не основной язык трансляции (основной должен стоять первым в списке selecta)
    var selected_data = document.getElementById("lang_select");
    if(selected_data.selectedIndex != 0){

        translation = undefined;

        if (HJPlayer.isSupported()) {
            var lang = selected_data.value;
            translation = new HJPlayer({
                type: 'flv',
                url: 'https://testcert.caravanofknowledge.com/live/livestream/{{$video_id}}_'+lang+'.flv',
            }, {
                //...user config
            });
            var audioElement = document.getElementById("audioElement");
            translation.attachMediaElement(audioElement);
            translation.load();
            player.mute();
            //запускаем аудио синхронного перевода
            translation.play();
        }
    }else{
        //если выбран основной язык, стоящий первый в списке selecta то просто воспроизводим ютую и выключаем переводы
        player.unMute();
        translation.pause();
        translation = undefined;
    }
}
</script>

<div id="ytplayer"></div> 
<br><br>
Синхронный перевод:<br>
<select style="padding:6px;" onChange="change_lang();" id="lang_select">
    <option value="0">Оригинал</option>
    <option value="ru">RU (русский)</option>
    <option value="kz">KZ (қазақща)</option>
    <option value="en">EN (english)</option>
</select>

<audio src="" id="audioElement"></audio>
<input type="hidden" id="translation_id" value="{{$video_id}}">

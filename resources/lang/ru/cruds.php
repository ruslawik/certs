<?php

return [
    'userManagement' => [
        'title'          => 'Управление пользователями',
        'title_singular' => 'Управление пользователями',
    ],
    'permission' => [
        'title'          => 'Разрешения',
        'title_singular' => 'Разрешение',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'title'             => 'Title',
            'title_helper'      => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'role' => [
        'title'          => 'Роли',
        'title_singular' => 'Роль',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'title'              => 'Title',
            'title_helper'       => ' ',
            'permissions'        => 'Permissions',
            'permissions_helper' => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
        ],
    ],
    'user' => [
        'title'          => 'Пользователи',
        'title_singular' => 'Пользователь',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => ' ',
            'name'                     => 'Name',
            'name_helper'              => ' ',
            'email'                    => 'Email',
            'email_helper'             => ' ',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => ' ',
            'password'                 => 'Password',
            'password_helper'          => ' ',
            'roles'                    => 'Roles',
            'roles_helper'             => ' ',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => ' ',
            'created_at'               => 'Created at',
            'created_at_helper'        => ' ',
            'updated_at'               => 'Updated at',
            'updated_at_helper'        => ' ',
            'deleted_at'               => 'Deleted at',
            'deleted_at_helper'        => ' ',
        ],
    ],
    'certificate' => [
        'title'          => 'Сертификаты',
        'title_singular' => 'Сертификаты',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Название',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'person' => [
        'title'          => 'Участники',
        'title_singular' => 'Участники',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'email'              => 'Email',
            'email_helper'       => ' ',
            'name'               => 'Имя Фамилия',
            'name_helper'        => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
            'certificate'        => 'Сертификат',
            'certificate_helper' => ' ',
            'cert_num'           => 'Номер сертификата',
            'cert_num_helper'    => 'будет сгенерирован автоматически при получении, заполнять не нужно',
            'bergen_date'        => 'Дата/Время получения',
            'bergen_date_helper' => 'будет выставлена автоматически при получении',
        ],
    ],
    'page' => [
        'title'          => 'Страницы сертификатов',
        'title_singular' => 'Страницы сертификатов',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'cert'               => 'Сертификат',
            'cert_helper'        => ' ',
            'image'              => 'Картинка для страницы',
            'image_helper'       => ' ',
            'orientation'        => 'Ориентация страницы',
            'orientation_helper' => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
            'number'             => 'Порядковый номер страницы',
            'number_helper'      => ' ',
        ],
    ],
    'item' => [
        'title'          => 'Объекты на страницах',
        'title_singular' => 'Объекты на страницах',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'type'              => 'Тип объекта на странице',
            'type_helper'       => ' ',
            'x_axis'            => 'Координаты по X',
            'x_axis_helper'     => 'Напишите -1 если нужно автоматически выравнивать по центру',
            'y_axis'            => 'Координаты по Y',
            'y_axis_helper'     => 'Напишите -1 если нужно автоматически выравнивать по центру',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
            'cert'              => 'Сертификат',
            'cert_helper'       => ' ',
            'page'              => 'Страница сертификата',
            'page_helper'       => ' ',
            'html_color'        => 'Html-код цвета',
            'font_size'         => 'Размер шрифта',
            'qr_size'           => 'Размер QR (длина стороны квадрата)',
        ],
    ],
];

<?php

namespace App\Http\Requests;

use App\Models\Person;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePersonRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('person_create');
    }

    public function rules()
    {
        return [
            'email' => [
                'required',
            ],
            'name' => [
                'string',
                'required',
            ],
            'certificate_id' => [
                'required',
                'integer',
            ],
            'cert_num' => [
                'string',
                'nullable',
            ],
            'bergen_date' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
        ];
    }
}

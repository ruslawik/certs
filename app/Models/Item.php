<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    use HasFactory;

    public const TYPE_SELECT = [
        'name'     => 'Имя и фамилия участника',
        'cert_num' => 'Номер сертификата',
        'qr'       => 'QR для проверки сертификата',
        'got_date'       => 'Дата получения сертификата',
    ];

    public $table = 'items';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'cert_id',
        'page_id',
        'type',
        'x_axis',
        'y_axis',
        'html_color',
        'font_size',
        'qr_size',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function cert()
    {
        return $this->belongsTo(Certificate::class, 'cert_id');
    }

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}

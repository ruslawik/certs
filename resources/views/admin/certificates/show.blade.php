@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.certificate.title') }} <a target="_blank" href="/admin/check-cert/{{$certificate->id}}">Просмотреть</a>
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.certificates.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.certificate.fields.id') }}
                        </th>
                        <td>
                            {{ $certificate->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.certificate.fields.name') }}
                        </th>
                        <td>
                            {{ $certificate->name }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.certificates.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#certificate_people" role="tab" data-toggle="tab">
                {{ trans('cruds.person.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#cert_pages" role="tab" data-toggle="tab">
                {{ trans('cruds.page.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="certificate_people">
            @includeIf('admin.certificates.relationships.certificatePeople', ['people' => $certificate->certificatePeople])
        </div>
        <div class="tab-pane" role="tabpanel" id="cert_pages">
            @includeIf('admin.certificates.relationships.certPages', ['pages' => $certificate->certPages])
        </div>
    </div>
</div>

@endsection
<script src="https://unpkg.com/peerjs@1.4.7/dist/peerjs.min.js"></script>
<script>
    /*
    var socket = new WebSocket('ws://127.0.0.1:2346');
    socket.binaryType = 'blob';
    navigator.mediaDevices.getUserMedia({
        audio: true
    })
    .then(function(stream) {
      var $audio = document.getElementById("audio");
      var mediarecorder = new MediaRecorder(stream, {mimeType: "audio/webm; codecs=\"opus\""});
      // send
      mediarecorder.ondataavailable = function(e) {
        if (e.data && e.data.size > 0) {
          var array = new Array(e.data);
          let mp3_chunk = new Blob(array, {type:'audio/wav'});
          socket.send(mp3_chunk);
        }
      }
      mediarecorder.start(1000);
    });*/

    var peer = new Peer();

    peer.on('open', function(peerID) {
        var peer_token = peerID;
        console.log(peer_token);
    });

    var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    peer.on('call', function(call) {
          console.log("income_call!");
          getUserMedia({video: false, audio: true}, function(stream) {
            call.answer(stream); // Answer the call with an A/V stream.
          }, function(err) {
            console.log('Failed to get local stream' ,err);
          });
    });

    /*
    peer.on('connection', function(dataConnection) {
        dataConnection.on('data', function(data) {
            
        });
    });*/

    /*
    const webSocket = new WebSocket('ws://127.0.0.1:2346');
    webSocket.binaryType = 'blob';

    webSocket.onopen = event => {
        console.log("hello! socket started");
        navigator.mediaDevices
            .getUserMedia({ audio: true, video: false })
            .then(stream => {
                const mediaRecorder = new MediaRecorder(stream, {
                    mimeType: 'audio/webm;codecs=pcm',
                });
                mediaRecorder.addEventListener('dataavailable', event => {
                    if (event.data.size > 0) {
                        webSocket.send(event.data);
                    }
                });
                mediaRecorder.start(3000);
            });
    };*/
</script>
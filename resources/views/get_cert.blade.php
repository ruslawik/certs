<html>
<title></title>
<head>
    @include("fonts.montserrat")
</head>
<body style="margin:0; padding:0;">
@foreach($pages as $page)
    @if($page->orientation == "land")
    <div style="margin:0;padding:0;position:relative;width:1130px;height:799px;background-image: url('{{$page->getImageAttribute()->url}}'); background-size:cover; font-family:'Montserrat';">
    @endif
    @if($page->orientation == "vert")
    <div style="margin:0;padding:0;position:relative;width:799px;height:1129px;background-image: url('{{$page->getImageAttribute()->url}}'); background-size:cover; font-family:'Montserrat';">
    @endif
        @foreach($page->getItems($page->id) as $item)
            @if($item->type == "name")
                <span style="font-size:{{$item->font_size}}; color:{{$item->html_color}}; position: absolute; @if($item->y_axis == -1) top: 50%; transform: translate(0, -50%); @else top:{{$item->y_axis}}; @endif @if($item->x_axis == -1) left: 50%; transform: translate(-50%); @else left:{{$item->x_axis}}; @endif display: block; font-weight: bold !important;">{{$name}}</span>
            @endif
            @if($item->type == "cert_num")
                <span style="font-size:{{$item->font_size}}; color:{{$item->html_color}}; position: absolute; @if($item->y_axis == -1) top: 50%; transform: translate(0, -50%); @else top:{{$item->y_axis}}; @endif @if($item->x_axis == -1) left: 50%; transform: translate(-50%); @else left:{{$item->x_axis}}; @endif display: block;">{{$cert_num}}</span>
            @endif
            @if($item->type == "qr")
                <span style="position: absolute; @if($item->y_axis == -1) top: 50%; transform: translate(0, -50%); @else top:{{$item->y_axis}}; @endif @if($item->x_axis == -1) left: 50%; transform: translate(-50%); @else left:{{$item->x_axis}}; @endif display: block;">
                    {!! QrCode::size($item->qr_size)->generate("https://certificates.caravanofknowledge.com/check/".$cert_id."/".$user_id); !!}
                </span>
            @endif
            @if($item->type == "got_date")
                <span style="font-size:{{$item->font_size}}; color:{{$item->html_color}}; position: absolute; @if($item->y_axis == -1) top: 50%; transform: translate(0, -50%); @else top:{{$item->y_axis}}; @endif @if($item->x_axis == -1) left: 50%; transform: translate(-50%); @else left:{{$item->x_axis}}; @endif display: block;">
                    <?php  $now_date = \Carbon\Carbon::now()->format('d/m/Y'); ?>
                    {{$now_date}}
                </span>
            @endif
        @endforeach
    </div>
@endforeach
</body>
</html>

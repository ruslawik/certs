<script src="https://unpkg.com/peerjs@1.4.7/dist/peerjs.min.js"></script>

<button onClick="call();">Call</button>
<script>
    var peer = new Peer();

    function call(){
        const audioCtx = new AudioContext();
        const dest = audioCtx.createMediaStreamDestination();

        var call = peer.call('{{$peer_id}}', dest.stream);

        call.on('stream', function(remoteStream) {
            console.log("streaming");
        });
    }

    /*
    var socket = new WebSocket('ws://127.0.0.1:2346');
    socket.binaryType = 'blob';
    navigator.mediaDevices.getUserMedia({
        audio: true
    })
    .then(function(stream) {
      var $audio = document.getElementById("audio");
      var mediarecorder = new MediaRecorder(stream, {mimeType: "audio/webm; codecs=\"opus\""});
      // send
      mediarecorder.ondataavailable = function(e) {
        if (e.data && e.data.size > 0) {
          var array = new Array(e.data);
          let mp3_chunk = new Blob(array, {type:'audio/wav'});
          socket.send(mp3_chunk);
        }
      }
      mediarecorder.start(1000);
    });*/
    /*
    const webSocket = new WebSocket('ws://127.0.0.1:2346');
    webSocket.binaryType = 'blob';

    webSocket.onopen = event => {
        console.log("hello! socket started");
        navigator.mediaDevices
            .getUserMedia({ audio: true, video: false })
            .then(stream => {
                const mediaRecorder = new MediaRecorder(stream, {
                    mimeType: 'audio/webm;codecs=pcm',
                });
                mediaRecorder.addEventListener('dataavailable', event => {
                    if (event.data.size > 0) {
                        webSocket.send(event.data);
                    }
                });
                mediaRecorder.start(3000);
            });
    };*/
</script>
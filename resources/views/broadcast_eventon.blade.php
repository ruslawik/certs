<script src="https://unpkg.com/peerjs@1.4.7/dist/peerjs.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" crossorigin="anonymous">
<script>
// Loads the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replaces the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
        height: '{{$height}}',
        width: '{{$width}}',
        videoId: '{{$video_id}}',
        playerVars: {
          autoplay: 1,
          controls: 1,
          disablekb: 1,
          hl: 'ru-ru',
          loop: 1,
          modestbranding: 1,
          showinfo: 0,
          autohide: 1,
          color: 'white',
          iv_load_policy: 3,
          theme: 'light',
          rel: 0
        }
        /*events: {
            'onReady': onPlayerReady,
        }*/
    });
  }


var current_lang = "or";
var peer = undefined;
var global_guest_bearer = 'Bearer {{$bearer_token}}';
var try_count = 0;
//var api_url = "http://127.0.0.1:8000";
var api_url = "https://eventon.caravanofknowledge.com";
var from_lang_id = 4;
var user_id = Math.floor(Math.random() * 9000) + 1;

function change_lang(){

    //Если старая была трансляция, убиваем
    if(typeof peer != "undefined"){
      peer.destroy();
      document.getElementById('streamAudio').src = "";
      document.getElementById('streamAudio').pause();
    }

    //Если выбран не основной язык трансляции (основной должен стоять первым в списке selecta)
    var selected_data = document.getElementById("lang_select");
    if(selected_data.selectedIndex != 0){

        var lang = selected_data.value;
        const lang_keys = {"kz":1, "ru":2, "en":3, "or":4};

        fetch(api_url+'/api/v1/audio?stream_id={{$stream_id}}&from_lang_id='+from_lang_id+'&to_lang_id='+lang_keys[lang], {
            headers: {
              'Authorization': global_guest_bearer
            }
        })
        .then((response) => {
            //Если токен протух или неверный, получаем новый
            if(response.status == 401){
                fetch(api_url+'/api/v1/auth/anonim').then((response) =>{response.json().then(function (json){
                    var bearer = json.token;
                    global_guest_bearer = "Bearer "+bearer;
                    console.log(global_guest_bearer);
                    //Если пытались менее 3 раз, пытаемся еще раз
                    if(try_count < 3){
                      change_lang();
                      try_count++;
                    }else{
                      document.getElementById("error").innerHTML = "Не удается подключиться к серверу переводов.";
                    }
                  });
                });
            }else{
              response.json().then(function (json){
               var translator_peer_id = json.data.audio_token;
               //Если токена переводчика нет, значит он не подключился
               if(translator_peer_id == null){
                  document.getElementById("error").innerHTML = "Переводчик отсутствует или приостановил эфир. <br>Выберите другой язык или оригинал трансляции";
               }else{
                  //Если токен пришел, окрываем свой peer и коннкетимся к переводчику
                  document.getElementById("error").innerHTML = "";

                  peer = new Peer();

                  peer.on('open', function(peerID) {
                    var peer_token = peerID;
                    //console.log(peer_token);
                    const audioCtx = new AudioContext();
                    const dest = audioCtx.createMediaStreamDestination();
                    //Звоним переводчику
                    var call = peer.call(translator_peer_id, dest.stream, {'metadata': {'user_id': user_id}});
                    //Запускаем стрим от переводчика
                    call.on('stream', function(remoteStream) {
                      //console.log("streaming translation");
                      player.mute();
                      setTimeout(function() {
                        document.getElementById('streamAudio').srcObject = remoteStream;
                        document.getElementById('streamAudio').onloadedmetadata= function(e) {
                            //Пытаемся проиграть запись  
                            var promise = document.getElementById('streamAudio').play();
                            //Если не получилось
                            if (promise !== undefined) {
                                promise.catch(error => {
                                    // Auto-play was prevented
                                    // Show a UI element to let the user manually start playback
                                    document.getElementById("play-button").style.display = "block";
                                    document.getElementById("error").innerHTML = "Нажмите на кнопку 'Слушать'";
                                }).then(() => {
                                    // Auto-play started
                                });
                            }
                        };
                      },1000);


                      let dataConnection = peer.connect(translator_peer_id);
                        dataConnection.on('open', function() {
                            console.log('dataConnection opened');
                            setInterval(() => dataConnection.send({'user_id': user_id}), 4000);
                      });

                    });

                  });
                }
              });
            }
        });
    }else{
        //если выбран основной язык, стоящий первый в списке selecta то просто воспроизводим ютуб
        player.unMute();
        document.getElementById("error").innerHTML = "";
    }
}

function play(){
  document.getElementById('streamAudio').play();
  document.getElementById("error").innerHTML = "";
}
</script>

<div id="ytplayer"></div> 
<br><br>
<div class="form-group col-sm-4">
  <div class="row"> 
    <div class="input-group input-group-md">
      <div class="input-group-prepend">
        <span class="input-group-text">Выберите перевод:</span>
      </div>
      <select class="form-control" style="padding:6px;" onChange="change_lang();" id="lang_select">
          <option value="0">Оригинал</option>
          <option value="kz">KZ (қазақща)</option>
          <option value="ru">RU (русский)</option>
          <option value="en">EN (english)</option>
      </select>
      <button class="col-sm-3 btn btn-success" id="play-button" style="display: none;" onClick="play();">Слушать</button>
    </div>
  </div>
</div>
<br>
<span id="error" style="color:red;"></span>

<audio src="" id="streamAudio" playsinline=""></audio>
<input type="hidden" id="translation_id" value="{{$video_id}}">
